<?php

namespace SamplePhpApp\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class HomeController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function index(Request $request, Response $response, array $args)
    {
        $content = '<p><a href="/users">Users</a></p>';
        $content .= '<p><a href="/users/1">User ID: 1</a></p>';
        $content .= '<p><a href="/users/2">User ID: 2</a></p>';
        $content .= '<p><a href="/users/3">User ID: 3</a></p>';
        $response->getBody()->write($content);
        return $response;
    }
}
